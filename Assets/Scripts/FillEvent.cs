﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FillEvent : MonoBehaviour {

    // Use this for initialization
    public TestEvents Myevent;
	private void OnEnable () {
        Myevent.SumEvent += SumNums;
        Myevent.SumEvent += SubNums;

    }
    private void OnDisable()
    {
        Myevent.SumEvent -= SumNums;
        Myevent.SumEvent -= SubNums;
    }
    

    // Update is called once per frame
    void Update () {
		
	}
    public void SumNums(float n1, float n2)
    {
        Debug.LogError("sum" + (n1 + n2));
        
        
    }
    public void SubNums(float n1, float n2)
    {
        Debug.LogError("sub"+(n1-n2));
        
    }

}
