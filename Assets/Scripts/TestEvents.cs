﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestEvents : MonoBehaviour {

    // Use this for initialization
    public delegate void Sum(float n1, float n2);
    public event Sum SumEvent;
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            if(SumEvent !=null)
            {
                SumEvent(2,3);
            }
            else
            {
                Debug.LogError("None");
            }
        }
		
	}

   
}
