﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

    // Use this for initialization
    public Transform CentreOfTheWorld;
    public float ForceAmount;
    private int SignAngle;
    private Rigidbody2D rb;
    public float JumpSpeed;
	void Start () {
        rb = GetComponent<Rigidbody2D>();
	}

    // Update is called once per frame
    private void Update()
    {
        if (Input.GetKey(KeyCode.RightArrow))
        {
            // rb.AddForce( * ForceAmount * Time.deltaTime, ForceMode2D.Force);
            rb.velocity = transform.right * ForceAmount * Time.deltaTime;
        }
        else if (Input.GetKeyUp(KeyCode.RightArrow))
        {
            rb.velocity = Vector2.zero;
        }
        else if (Input.GetKeyDown(KeyCode.Space))
        {
            rb.velocity = transform.up * JumpSpeed * Time.deltaTime;
        }
        else if (Input.GetKeyUp(KeyCode.Space))
        {
            rb.velocity = Vector2.zero;
        }



    }
    void FixedUpdate () {
        SignAngle = Vector3.Cross(CentreOfTheWorld.up, transform.position - CentreOfTheWorld.position).z > 0 ? 1 : -1;
        Debug.LogError(Vector2.Angle(CentreOfTheWorld.up,transform.position-CentreOfTheWorld.position) * SignAngle);
        rb.transform.localRotation = Quaternion.AngleAxis( Vector2.Angle(CentreOfTheWorld.up, transform.position - CentreOfTheWorld.position) * SignAngle,Vector3.forward);
        rb.AddForce(new Vector2((CentreOfTheWorld.position.x - transform.position.x), CentreOfTheWorld.position.y - transform.position.y) * ForceAmount * Time.deltaTime, ForceMode2D.Force);








    }
}
